package com.rijks.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.rijks.TestCoroutineRule
import com.rijks.api.CollectionApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper: CollectionApi

    private lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
        viewModel = MainViewModel(apiHelper)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun fetchCollections_called() {

        runBlockingTest {
            //Given
            val apiKey = "xlI0GHBH"
            val involvedMaker = "Rembrandt van Rijn"

            //When
            viewModel.fetchCollections(apiKey,involvedMaker)

            //Then
            Mockito.verify(apiHelper, times(1)).getCollections(apiKey,involvedMaker)
        }
    }

}