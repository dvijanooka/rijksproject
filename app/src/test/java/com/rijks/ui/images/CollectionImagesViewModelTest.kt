package com.rijks.ui.images

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.rijks.TestCoroutineRule
import com.rijks.api.CollectionApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CollectionImagesViewModelTest{
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper: CollectionApi

    private lateinit var viewModel:CollectionImagesViewModel

    @Before
    fun setUp() {
        viewModel = CollectionImagesViewModel(apiHelper)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun fetchImages_called() {

        runBlockingTest {
            //Given
            val id = "SK-C-5"
            val apiKey = "xlI0GHBH"

            //When
            viewModel.fetchImages(id,apiKey)

            //Then
            Mockito.verify(apiHelper, Mockito.times(1)).getCollectionImages(id,apiKey)
        }
    }
}