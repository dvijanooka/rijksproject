package com.rijks.model

import com.google.gson.annotations.SerializedName

data class CollectionDetailObject(

    @SerializedName("elapsedMilliseconds") val elapsedMilliseconds: Int,
    @SerializedName("artObject") val artObject: ArtObject?,
    @SerializedName("artObjectPage") val artObjectPage: ArtObjectPage
)