package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Acquisition(
    @SerializedName("method") val method: String,
    @SerializedName("date") val date: String,
    @SerializedName("creditLine") val creditLine: String
)
