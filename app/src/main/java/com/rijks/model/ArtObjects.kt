package com.rijks.model

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat


data class ArtObjects(
    @SerializedName("links") val links: Links,
    @SerializedName("id") val id: String,
    @SerializedName("objectNumber") val objectNumber: String,
    @SerializedName("title") val title: String,
    @SerializedName("hasImage") val hasImage: Boolean,
    @SerializedName("principalOrFirstMaker") val principalOrFirstMaker: String,
    @SerializedName("longTitle") val longTitle: String,
    @SerializedName("showImage") val showImage: Boolean,
    @SerializedName("permitDownload") val permitDownload: Boolean,
    @SerializedName("webImage") val webImage: WebImage,
    @SerializedName("headerImage") val headerImage: HeaderImage,
    @SerializedName("productionPlaces") val productionPlaces: List<String>
)

@BindingAdapter("collectionImage")
fun loadImage(view: ImageView, imageUrl: String?) {
    val url = imageUrl?.replace("http://","https://")
    Glide.with(view.context)
        .load(url)
        .into(view)
}

@SuppressLint("SimpleDateFormat")
@BindingAdapter("bindDate")
fun bindDate(textView: TextView, dateToFormat: String?) {
    if (dateToFormat != null) {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val date = sdf.parse(dateToFormat)
        if (date != null) {
            val convertedDate = SimpleDateFormat("dd MMM yyyy").format(date)
            textView.text = convertedDate.toString()
        }
    }
}

@BindingAdapter("layout_height")
fun setLayoutHeight(view: ImageView, height: Long) {
    val layoutParams: ViewGroup.LayoutParams = view.layoutParams
    layoutParams.height = height.toInt()
    view.setLayoutParams(layoutParams)
}

@BindingAdapter("layout_width")
fun setLayoutWidth(view: ImageView, width: Long) {
    val layoutParams: ViewGroup.LayoutParams = view.layoutParams
    layoutParams.width = width.toInt()
    view.setLayoutParams(layoutParams)
}
