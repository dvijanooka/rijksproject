package com.rijks.model

import com.google.gson.annotations.SerializedName

data class PrincipleMakers (

	@SerializedName("name") val name : String,
	@SerializedName("unFixedName") val unFixedName : String,
	@SerializedName("placeOfBirth") val placeOfBirth : String,
	@SerializedName("dateOfBirth") val dateOfBirth : String,
	@SerializedName("dateOfBirthPrecision") val dateOfBirthPrecision : String,
	@SerializedName("dateOfDeath") val dateOfDeath : String,
	@SerializedName("dateOfDeathPrecision") val dateOfDeathPrecision : String,
	@SerializedName("placeOfDeath") val placeOfDeath : String,
	@SerializedName("occupation") val occupation : List<String>,
	@SerializedName("roles") val roles : List<String>,
	@SerializedName("nationality") val nationality : String,
	@SerializedName("biography") val biography : String,
	@SerializedName("productionPlaces") val productionPlaces : List<String>,
	@SerializedName("qualification") val qualification : String,
	@SerializedName("labelDesc") val labelDesc : String
)