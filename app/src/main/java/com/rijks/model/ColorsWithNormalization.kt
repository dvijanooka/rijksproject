package com.rijks.model

import com.google.gson.annotations.SerializedName

data class ColorsWithNormalization (

	@SerializedName("originalHex") val originalHex : String,
	@SerializedName("normalizedHex") val normalizedHex : String
)