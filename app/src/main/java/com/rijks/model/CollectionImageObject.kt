package com.rijks.model
import com.google.gson.annotations.SerializedName

data class CollectionImageObject (

    @SerializedName("levels") val levels : List<Levels>?,
)