package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Levels(
    @SerializedName("name") val name: String,
    @SerializedName("width") val width: Long,
    @SerializedName("height") val height: Long,
    @SerializedName("tiles") val tiles: List<Tiles>,
)
