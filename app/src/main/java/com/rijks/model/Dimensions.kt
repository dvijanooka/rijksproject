package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Dimensions (

	@SerializedName("unit") val unit : String,
	@SerializedName("type") val type : String,
	@SerializedName("part") val part : String,
	@SerializedName("value") val value : String
)