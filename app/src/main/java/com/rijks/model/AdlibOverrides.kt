package com.rijks.model

import com.google.gson.annotations.SerializedName

data class AdlibOverrides (

	@SerializedName("titel") val titel : String,
	@SerializedName("maker") val maker : String,
	@SerializedName("etiketText") val etiketText : String
)