package com.rijks.model

import com.google.gson.annotations.SerializedName

data class ArtObjectPage (

	@SerializedName("id") val id : String,
	@SerializedName("similarPages") val similarPages : List<String>,
	@SerializedName("lang") val lang : String,
	@SerializedName("objectNumber") val objectNumber : String,
	@SerializedName("tags") val tags : List<String>,
	@SerializedName("plaqueDescription") val plaqueDescription : String,
	@SerializedName("audioFile1") val audioFile1 : String,
	@SerializedName("audioFileLabel1") val audioFileLabel1 : String,
	@SerializedName("audioFileLabel2") val audioFileLabel2 : String,
	@SerializedName("createdOn") val createdOn : String,
	@SerializedName("updatedOn") val updatedOn : String,
	@SerializedName("adlibOverrides") val adlibOverrides : AdlibOverrides
)