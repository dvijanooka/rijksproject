package com.rijks.model
import com.google.gson.annotations.SerializedName

data class CollectionObject (

    @SerializedName("elapsedMilliseconds") val elapsedMilliseconds : Int,
    @SerializedName("count") val count : Int,
    @SerializedName("countFacets") val countFacets : CountFacets,
    @SerializedName("artObjects") val artObjects : List<ArtObjects>?,
    @SerializedName("facets") val facets : List<Facets>
)