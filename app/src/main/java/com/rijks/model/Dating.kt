package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Dating(

    @SerializedName("presentingDate") val presentingDate: String,
    @SerializedName("sortingDate") val sortingDate: Int,
    @SerializedName("period") val period: Int,
    @SerializedName("yearEarly") val yearEarly: Int,
    @SerializedName("yearLate") val yearLate: Int
)