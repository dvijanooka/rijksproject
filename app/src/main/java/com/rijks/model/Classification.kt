package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Classification (

	@SerializedName("iconClassIdentifier") val iconClassIdentifier : List<String>,
	@SerializedName("iconClassDescription") val iconClassDescription : List<String>,
	@SerializedName("motifs") val motifs : List<String>,
	@SerializedName("events") val events : List<String>,
	@SerializedName("periods") val periods : List<String>,
	@SerializedName("places") val places : List<String>,
	@SerializedName("people") val people : List<String>,
	@SerializedName("objectNumbers") val objectNumbers : List<String>
)