package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Tiles(
    @SerializedName("x") val x: Int,
    @SerializedName("y") val y: Int,
    @SerializedName("url") val url: String
)