package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Colors (

	@SerializedName("percentage") val percentage : Int,
	@SerializedName("hex") val hex : String
)