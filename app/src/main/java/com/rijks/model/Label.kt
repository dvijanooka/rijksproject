package com.rijks.model

import com.google.gson.annotations.SerializedName

data class Label (

	@SerializedName("title") val title : String,
	@SerializedName("makerLine") val makerLine : String,
	@SerializedName("description") val description : String,
	@SerializedName("notes") val notes : String,
	@SerializedName("date") val date : String
)