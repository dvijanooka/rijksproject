package com.rijks.model

import com.google.gson.annotations.SerializedName
import com.rijks.model.FacetObject

data class Facets (

    @SerializedName("facets") val facets : List<FacetObject>,
    @SerializedName("name") val name : String,
    @SerializedName("otherTerms") val otherTerms : Int,
    @SerializedName("prettyName") val prettyName : Int
)