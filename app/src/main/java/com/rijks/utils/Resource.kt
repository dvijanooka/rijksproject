package com.rijks.utils

data class Resource<out T>(val status: Status, val data: T?, val messageId: Int?) {

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, 0)
        }

        fun <T> error(msgId: Int, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msgId)
        }

    }

}