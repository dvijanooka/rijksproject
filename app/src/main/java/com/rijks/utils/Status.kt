package com.rijks.utils

enum class Status {
    SUCCESS,
    ERROR
}