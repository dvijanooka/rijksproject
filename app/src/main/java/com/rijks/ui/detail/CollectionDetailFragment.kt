package com.rijks.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.rijks.utils.Status
import com.rijks.Constants
import com.rijks.Constants.Companion.COLLECTION_ID
import com.rijks.MainActivity
import com.rijks.R
import com.rijks.databinding.CollectionDetailFragmentBinding
import com.rijks.model.CollectionDetailObject
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CollectionDetailFragment : Fragment() {

    private lateinit var binding: CollectionDetailFragmentBinding
    private val viewModel: CollectionDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CollectionDetailFragmentBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val id = arguments?.getString(COLLECTION_ID)
        binding.viewModel = viewModel

        if(viewModel.collectionDetails.value == null) {
            viewModel.fetchCollectionDetail(id, Constants.API_KEY)
        }

        viewModel.collectionDetails.observe(viewLifecycleOwner) { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    binding.mainConstraint.visibility = View.VISIBLE
                    binding.errorTextView.visibility = View.GONE
                    if (result.data is CollectionDetailObject) {
                        showDetails(result.data)
                    }
                }
                Status.ERROR -> {
                    binding.mainConstraint.visibility = View.GONE
                    binding.errorTextView.visibility = View.VISIBLE
                }
            }
        }

        binding.collectionImage.setOnClickListener {
            val args = Bundle().apply {
                putString(COLLECTION_ID, id)
            }
            view.let { Navigation.findNavController(it).navigate(R.id.imagesFragment, args) }

        }
    }

    private fun showDetails(collectionDetails: CollectionDetailObject?) {
        if (collectionDetails != null) {
            binding.model = collectionDetails.artObject
            val principleMakers = collectionDetails.artObject?.principalMakers
            if (principleMakers != null && principleMakers.isNotEmpty()) {
                binding.birthModel = principleMakers[0]
            }
        }
    }

    override fun onResume() {
        super.onResume()

        (activity as MainActivity?)?.editActionBarTitle(true)
    }
}