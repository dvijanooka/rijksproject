package com.rijks.ui.detail

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rijks.utils.Resource
import com.rijks.R
import com.rijks.api.CollectionApi
import com.rijks.model.CollectionDetailObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CollectionDetailViewModel @Inject constructor(
    private val collectionApi: CollectionApi
) : ViewModel() {

    private val collectionDetailsLiveData = MutableLiveData<Resource<CollectionDetailObject>>()
    val collectionDetails: LiveData<Resource<CollectionDetailObject>> = collectionDetailsLiveData

    private val loadingLiveData = MutableLiveData<Int>()
    val loading: LiveData<Int> = loadingLiveData

    fun fetchCollectionDetail(id: String?,key:String) {
        if (id != null) {
            loadingLiveData.postValue(View.VISIBLE)
            viewModelScope.launch {
                val collectionDetails = collectionApi.getCollectionDetails(id,key)
                loadingLiveData.postValue(View.GONE)
                try {
                    if (collectionDetails?.artObject != null) {
                        collectionDetailsLiveData.postValue(Resource.success(collectionDetails))

                    } else {
                        collectionDetailsLiveData.postValue(
                            Resource.error(
                                R.string.error_message,
                                null
                            )
                        )
                    }
                } catch (e: Exception) {
                    collectionDetailsLiveData.postValue(
                        Resource.error(
                            R.string.error_message,
                            null
                        )
                    )
                }
            }
        } else {
            collectionDetailsLiveData.postValue(
                Resource.error(
                    R.string.error_message,
                    null
                )
            )
        }
    }

}