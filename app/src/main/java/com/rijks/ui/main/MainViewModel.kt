package com.rijks.ui.main

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rijks.utils.Resource
import com.rijks.R
import com.rijks.api.CollectionApi
import com.rijks.model.CollectionObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val collectionApi: CollectionApi
) : ViewModel() {

    private val collectionsLiveData = MutableLiveData<Resource<CollectionObject>>()
    val collections: LiveData<Resource<CollectionObject>> = collectionsLiveData

    private val loadingLiveData = MutableLiveData<Int>()
    val loading: LiveData<Int> = loadingLiveData

    fun fetchCollections(apiKey: String, involvedMaker: String) {
        loadingLiveData.postValue(View.VISIBLE)
        viewModelScope.launch {
            val collections =
                collectionApi.getCollections(apiKey, involvedMaker)

            loadingLiveData.postValue(View.GONE)
            try {
                if (collections?.artObjects != null && collections.artObjects.isNotEmpty()) {
                    collectionsLiveData.postValue(Resource.success(collections))
                } else {
                    collectionsLiveData.postValue(Resource.error(R.string.error_message, null))
                }
            } catch (e: Exception) {
                collectionsLiveData.postValue(Resource.error(R.string.error_message, null))
            }

        }
    }
}