package com.rijks.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.rijks.utils.Status
import com.rijks.Constants
import com.rijks.Constants.Companion.COLLECTION_ID
import com.rijks.MainActivity
import com.rijks.R
import com.rijks.adapters.CollectionsAdapter
import com.rijks.databinding.MainFragmentBinding
import com.rijks.model.ArtObjects
import com.rijks.model.CollectionObject
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(), CollectionsAdapter.OnClickListener {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: MainFragmentBinding
    private val adapter = CollectionsAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            collectionsRecycler.apply {
                layoutManager = GridLayoutManager(activity,2)
            }
        }

        binding.viewModel = viewModel

        binding.adapter = adapter

        adapter.setClickListener(this)

        if(viewModel.collections.value == null) {
            viewModel.fetchCollections(Constants.API_KEY, Constants.INVOLVED_MAKER)
        }

        viewModel.collections.observe(viewLifecycleOwner) { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    binding.collectionsRecycler.visibility = View.VISIBLE
                    binding.errorTextView.visibility = View.GONE
                    if (result.data is CollectionObject) {
                        result.data.artObjects?.let { renderList(it) }
                    }
                }
                Status.ERROR -> {
                    binding.collectionsRecycler.visibility = View.GONE
                    binding.errorTextView.visibility = View.VISIBLE
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(users: List<ArtObjects>) {
        adapter.addData(users)
        adapter.notifyDataSetChanged()
    }

    override fun onClick(artObjects: ArtObjects) {
        val args = Bundle().apply {
            putString(COLLECTION_ID, artObjects.objectNumber)
        }
        view?.let { Navigation.findNavController(it).navigate(R.id.detailFragment, args) }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity?)?.editActionBarTitle(false)
    }

}