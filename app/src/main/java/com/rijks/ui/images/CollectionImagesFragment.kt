package com.rijks.ui.images

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.rijks.utils.Status
import com.rijks.Constants
import com.rijks.Constants.Companion.COLLECTION_ID
import com.rijks.MainActivity
import com.rijks.adapters.CollectionsImagesAdapter
import com.rijks.databinding.CollectionImagesFragmentBinding
import com.rijks.model.CollectionImageObject
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class CollectionImagesFragment : Fragment() {

    private lateinit var binding: CollectionImagesFragmentBinding
    private val viewModel: CollectionImagesViewModel by viewModels()
    private val adapter = CollectionsImagesAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CollectionImagesFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val id = arguments?.getString(COLLECTION_ID)

        binding.viewModel = viewModel

        viewModel.fetchImages(id,Constants.API_KEY)

        viewModel.collectionImages.observe(viewLifecycleOwner) { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    binding.collectionImagesRecycler.visibility = View.VISIBLE
                    binding.errorTextView.visibility = View.GONE
                    if (result.data is CollectionImageObject) {
                        showImages(result.data)
                    }
                }
                Status.ERROR -> {
                    binding.collectionImagesRecycler.visibility = View.GONE
                    binding.errorTextView.visibility = View.VISIBLE
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showImages(collectionImages: CollectionImageObject?) {
        if (collectionImages?.levels != null && collectionImages.levels.isNotEmpty()) {
            val level0 = collectionImages.levels[4]
            var tiles = level0.tiles

            tiles = tiles.sortedBy { it.x }
            tiles = tiles.sortedBy { it.y }

            // to get the max value of x in tiles and use it for the number of columns in grid
            val column = tiles.stream().max(Comparator.comparing { v -> v.x }).get().x

            adapter.addData(level0, tiles)

            binding.apply {
                collectionImagesRecycler.apply {
                    layoutManager = GridLayoutManager(activity, column + 1)
                }
            }

            binding.adapter = adapter
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity?)?.editActionBarTitle(true)
    }
}