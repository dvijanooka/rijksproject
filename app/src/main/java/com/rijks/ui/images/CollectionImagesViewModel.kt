package com.rijks.ui.images

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rijks.utils.Resource
import com.rijks.R
import com.rijks.api.CollectionApi
import com.rijks.model.CollectionImageObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CollectionImagesViewModel @Inject constructor(
    private val collectionApi: CollectionApi
) : ViewModel() {

    private val collectionImagesLiveData = MutableLiveData<Resource<CollectionImageObject>>()
    val collectionImages: LiveData<Resource<CollectionImageObject>> = collectionImagesLiveData

    private val loadingLiveData = MutableLiveData<Int>()
    val loading: LiveData<Int> = loadingLiveData

    fun fetchImages(id: String?, key: String) {
        if (id != null) {
            loadingLiveData.postValue(View.VISIBLE)
            viewModelScope.launch {
                val collectionImages = collectionApi.getCollectionImages(id, key)
                loadingLiveData.postValue(View.GONE)
                try {
                    if (collectionImages != null && collectionImages.levels?.isNotEmpty() == true) {
                        collectionImagesLiveData.postValue(Resource.success(collectionImages))
                    } else {
                        collectionImagesLiveData.postValue(
                            Resource.error(
                                R.string.error_load_images,
                                null
                            )
                        )
                    }

                } catch (e: Exception) {
                    collectionImagesLiveData.postValue(
                        Resource.error(
                            R.string.error_load_images,
                            null
                        )
                    )
                }
            }
        } else {
            collectionImagesLiveData.postValue(
                Resource.error(
                    R.string.error_load_images,
                    null
                )
            )
        }
    }
}