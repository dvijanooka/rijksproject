package com.rijks

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val actionbar = supportActionBar
        actionbar?.title = resources.getString(R.string.app_name)
        actionbar?.setDisplayHomeAsUpEnabled(false)
        actionbar?.setHomeButtonEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment)
                val backStackEntryCount = navHostFragment?.childFragmentManager?.backStackEntryCount
                if (backStackEntryCount != null && backStackEntryCount > 1) {
                    navHostFragment.childFragmentManager.popBackStack()
                } else {
                    onSupportNavigateUp()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun editActionBarTitle(showHome: Boolean) {
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(showHome)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        Navigation.findNavController(this, R.id.navHostFragment).navigateUp()
        return true
    }

}