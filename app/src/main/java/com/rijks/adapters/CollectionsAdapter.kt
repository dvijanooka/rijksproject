package com.rijks.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rijks.databinding.CollectionItemLayoutBinding
import com.rijks.model.ArtObjects

class CollectionsAdapter(
    private val collectionObjects: ArrayList<ArtObjects>
) : RecyclerView.Adapter<CollectionsAdapter.DataViewHolder>() {

    interface OnClickListener {
        fun onClick(artObjects: ArtObjects)
    }

    private var mClickListener: OnClickListener? = null

    class DataViewHolder(val binding: CollectionItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setClickListener(callback: OnClickListener) {
        mClickListener = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            CollectionItemLayoutBinding.inflate(LayoutInflater.from(parent.context))
        )

    override fun getItemCount(): Int = collectionObjects.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {

        holder.binding.model = collectionObjects[position]
        holder.binding.executePendingBindings()

        holder.binding.root.setOnClickListener {
            mClickListener?.onClick(collectionObjects[position])
        }
    }

    fun addData(list: List<ArtObjects>) {
        collectionObjects.addAll(list)
    }

}

