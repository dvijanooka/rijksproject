package com.rijks.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rijks.databinding.CollectionImageLayoutBinding
import com.rijks.model.Levels
import com.rijks.model.Tiles

class CollectionsImagesAdapter(
    private val collectionImages: ArrayList<Tiles>
) : RecyclerView.Adapter<CollectionsImagesAdapter.DataViewHolder>() {

    private lateinit var level: Levels

    class DataViewHolder(val binding: CollectionImageLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            CollectionImageLayoutBinding.inflate(LayoutInflater.from(parent.context))
        )

    override fun getItemCount(): Int = collectionImages.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {

        holder.binding.model = collectionImages[position]
        holder.binding.executePendingBindings()
    }

    fun addData(level: Levels, list: List<Tiles>) {
        this.level = level
        collectionImages.addAll(list)
    }

}

