package com.rijks

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class SplashActivity : AppCompatActivity() {

    private val mHandler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mHandler.postDelayed({
            try {
                val i = Intent(
                    this,
                    MainActivity::class.java
                )
                startActivity(i)
                finish()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }, 3000)
    }
}