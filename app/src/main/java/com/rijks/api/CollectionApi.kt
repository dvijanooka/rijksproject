package com.rijks.api
import com.rijks.model.CollectionDetailObject
import com.rijks.model.CollectionImageObject
import com.rijks.model.CollectionObject
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CollectionApi {

    companion object{
        const val BASE_URL="https://www.rijksmuseum.nl/api/nl/"
    }

    @GET("collection?")
    suspend fun getCollections(
        @Query("key") key: String,
        @Query("involvedMaker") involvedMaker: String
    ): CollectionObject?

    @GET("collection/{id}")
    suspend fun getCollectionDetails(
        @Path(value = "id") id: String,
        @Query(value = "key") key: String
    ): CollectionDetailObject?

    @GET("collection/{id}/tiles?")
    suspend fun getCollectionImages(
        @Path(value = "id") id: String,
        @Query(value = "key") key: String
    ): CollectionImageObject?

}